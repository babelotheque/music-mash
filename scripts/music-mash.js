//If JS present, begin title animation by resetting head to underscore
document.getElementById('innerTitle').innerHTML = '_';

//Define 'typing' animation for title
function titleType(){

  //Title string and animation delay variable declaration
  var mainTitleSrc = 'Music Mash.';
  var mainTitleTypeDelay = 100;
  var mainTitleBlinkDelay = 500;

  //Prepare variables for animation process
  var mainTitleArray = mainTitleSrc.split('');
  var mainTitleCurrent = '';
  var mainTitleCount = 0;
  var mainTitleFinalChar = '_';

  //Write current title text to HTML, with or without underscore
  function writeTitle(){
    document.getElementById('innerTitle').innerHTML = (mainTitleCurrent + mainTitleFinalChar);
  };

  //Gradually build current title text to final string, character by character, writing to screen after each
  setInterval(function(){
    if (mainTitleCount < mainTitleArray.length) {
      mainTitleCurrent += mainTitleArray[mainTitleCount];
      writeTitle();
      mainTitleCount++
    } else {
      clearInterval();
    };
  }, mainTitleTypeDelay);

  //Once title written, begin blinking animation for final underscore indefinitely
  setInterval(function(){
    if (mainTitleFinalChar == '_') {
      mainTitleFinalChar = '';
    } else {
      mainTitleFinalChar = '_';
    };
    writeTitle();
  }, mainTitleBlinkDelay);
};

titleType();
